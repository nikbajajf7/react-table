export const budgetPrices = [
    {
        label: "$1-10",
        value: "$1-10"
    },
    {
        label: "$11-20",
        value: "$11-20"
    },
    {
        label: "$20-50",
        value: "$20-50"
    },
]

export const premiumPrices = [
    {
        label: "$50-99",
        value: "$50-99"
    },
    {
        label: "$100-199",
        value: "$100-199"
    },
    {
        label: "$200+",
        value: "$200+"
    },
]