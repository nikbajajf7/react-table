import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import Select from "react-select"
import { updateItem, createItem, setSelectedProduct } from "../reducers/table"

import "../styles/editProduct.css"
import { budgetPrices, premiumPrices } from "../utils/constants"

function EditProduct({mode}) {
    const [values, setValues] = useState({})
    const dispatch = useDispatch()
    const history = useHistory()
    const {selectedItem, items} = useSelector(state => state.tableItems)

    useEffect(() => {
        if(mode === "edit"){
            if(!selectedItem) {
                history.push("/")
            }
            const item = items.find((item) => (item._id === selectedItem))
            const itemCost = (item && item.price_tier === "budget" ? budgetPrices : premiumPrices).find((price) => price.value === (item && item.price_range))
            setValues({...item, price: itemCost})
            return() => {
                setValues({})
                dispatch(setSelectedProduct({ itemId: null }))
            }
        }
    }, [selectedItem])

    const isSubmitButtonDisable = () => {
        const requiredFields = ["product_name", "weight", "url", "price_tier"]
        let isTrue = requiredFields.find((field) => {
            if(typeof values[field] === "string"){
               if(!values[field].length) {
                    return true
                }
                return false
            } else if(values[field] === undefined){
                return true
            }
        })
        return (isTrue || !values.price) ? true : false    
    }

    const handleOnChange = (e) => {
        const {name, value, type} = e.target
        if (type === "checkbox") {
            setValues(state => ({
                ...state,
                [name]: state[name] ? false : true
            }))
        } else {
            setValues(state => ({
                ...state,
                [name]: value
            }))
        }
    }

    const handlePriceRangeChange = (value) => {
        setValues(state => ({
            ...state,
            price: value
        }))
    }

    const handleSubmit = () => {
        const data = {...values}
        delete data.price
        if (mode === "edit") {
            dispatch(updateItem({ item: {
                ...data,
                price_range: values.price.value
            }}))
        } else {
            dispatch(createItem({ item: {
                ...data,
                price_range: values.price.value,
                _id: items.length +1,
                unit_cost: null,
            }}))
        }
        history.push("/")
    }

    const handleCancel = () => {
        history.push("/")
    }

    return(
        <div className="edit-product">
            <h3 className="header">
                {mode === "create" ? "Create" : "Edit"} Product
            </h3>
            <div className="divider" />
            <div className="edit-product-form">
                <div className="form-left">
                    <div className="field">
                        <label className="field-key">Name</label>
                        <input
                            type="text"
                            className="field-value input-field"
                            value={values.product_name}
                            name="product_name"
                            onChange={handleOnChange}
                        />
                    </div>
                    <div className="field">
                        <label className="field-key">Availability</label>
                        <input
                            type="number"
                            min={0}
                            className="field-value input-field"
                            value={values.availability}
                            name="availability"
                            onChange={handleOnChange}
                        />
                    </div>
                    <div className="field">
                        <label className="field-key">Price Tier</label>
                        <div className="field-value radio-button">
                            <div className="radio">
                                <label className="mr-10 radio-label">
                                    <input
                                        type="radio"
                                        value="budget"
                                        checked={values.price_tier === "budget"}
                                        className="mr-5"
                                        name="price_tier"
                                        onChange={handleOnChange}
                                    />
                                    Budget
                                </label>
                            </div>
                            <div className="radio">
                                <label className="radio-label">
                                    <input
                                        type="radio"
                                        value="premium"
                                        className="mr-5"
                                        checked={values.price_tier === "premium"}
                                        name="price_tier"
                                        onChange={handleOnChange}
                                    />
                                    Premier
                                </label>
                            </div>
                        </div>
                    </div>                                        
                </div>
                <div className="form-right">
                    <div className="field">
                        <label className="field-key">Weight</label>
                        <input
                        type="text"
                        className="field-value input-field"
                        value={values.weight}
                        name="weight"
                        onChange={handleOnChange}
                    />
                    </div>
                    <div className="field">
                        <label className="field-key">Product Url</label>
                        <input
                            type="text"
                            className="field-value input-field"
                            value={values.url}
                            name="url"
                            onChange={handleOnChange}
                        />
                    </div>
                    <div>
                    <div className="field">
                        <label className="field-key">Price Range</label>
                        <div className="field-value">
                            <Select
                                className="select-field"
                                value={values.price}
                                options={values.price_tier === "budget" ? budgetPrices : premiumPrices}
                                onChange={handlePriceRangeChange}
                            />
                        </div>
                    </div>
                    <div className="checkbox-field">
                        <input
                            type="checkbox"
                            className="mr-10"
                            checked={values.isEditable}
                            name="isEditable"
                            onChange={handleOnChange}
                        />
                        <label className="field-key">Make product editable</label>
                    </div>
                </div>
                </div>
            </div>
            <div className="buttons">
                <button
                    type="submit"
                    className="cancle-button"
                    onClick={handleCancel}
                >
                    Cancel
                    </button>
                <button
                    type="submit"
                    className="submit-button"
                    disabled={isSubmitButtonDisable()}
                    onClick={handleSubmit}
                >
                    Submit
                </button>
                </div>
        </div>
    )
}

export default EditProduct