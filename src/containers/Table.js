import React, { useEffect } from "react"
import "../styles/table.css"
import { useDispatch, useSelector } from "react-redux"
import { deleteItem, getItems, setSelectedProduct } from "../reducers/table"
import { useHistory } from "react-router"

function Table() {
    const { items = [] } = useSelector(state => state.tableItems)
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        if(!items.length){
            dispatch(getItems())
        }
    }, [])

    const onEditClick = (id) => {
        dispatch(setSelectedProduct({ itemId: id }))
        history.push("/edit-product")
    }

    const openCreateForm = () => {
        history.push("/create-product")
    }

    const handleDeleteItem = (itemId) => [
        dispatch(deleteItem({ itemId }))
    ]

    return(
        <div className="container">
            <div className="table">
                <div className="add-product">
                    <h3 className="products-label">Products</h3>
                    <button
                        className="add-button"
                        onClick={openCreateForm}
                    >
                        Add Product
                    </button>
                </div>
                <div className="table-header">
                    <h3 className="item-name column-name">Name</h3>
                    <h3 className="item-weight column-name">Weight</h3>
                    <h3 className="item-availability column-name">Availability</h3>
                    <h3 className="item-editable column-name">Actions</h3>
                </div>
                <div className="divider"/>
                <div className="table-body">
                    {items.map((item) => (
                        <div className="table-row" key={item._id}>
                            <label className="item-name">{item.product_name}</label>
                            <label className="item-weight">{item.weight}</label>
                            <label className="item-availability">{item.availability}</label>
                            <div className="item-editable">
                                {item.isEditable ?
                                    (<button className="edit-button" onClick={() => onEditClick(item._id)}>
                                        Edit
                                    </button>) : (<div className="edit-button-space"/>)
                                }
                                <button
                                    className="delete-button"
                                    onClick={() => handleDeleteItem(item._id)}
                                >
                                    Delete
                                </button>
                            </div>
                        </div>
                    ))}
                
                </div>
            </div>
        </div>
    )
}

export default Table