import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Table from "./containers/Table"
import EditAndCreateProduct from "./containers/EditProduct"

function App() {
  return (
    <Router>
        <Switch>
          <Route path="/" exact={true}>
            <Table />
          </Route>
          <Route path="/edit-product" exact={true}>
            <EditAndCreateProduct mode="edit"/>
          </Route>
          <Route path="/create-product" exact={true}>
            <EditAndCreateProduct mode="create"/>
          </Route>
        </Switch>
    </Router>
  );
}

export default App
