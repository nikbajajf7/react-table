import { configureStore } from '@reduxjs/toolkit';
import tableReducer from './table';

export const store = configureStore({
  reducer: {
    tableItems: tableReducer,
  },
});
