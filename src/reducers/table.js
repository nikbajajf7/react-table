import { createSlice } from '@reduxjs/toolkit';
import data from "../productData.json" 

const initialState = {
  items: [],
  selectedItem: null,
};

export const table = createSlice({
    name: 'tableItems',
    initialState,
    reducers: {
        getItems: (state) => {
            return ({ ...state, items: data })
        },
        setSelectedProduct: (state, action) => {
            const {payload = {}} = action
            return({
                ...state,
                selectedItem: payload.itemId
            })
        },
        updateItem: (state, action) => {
            const { payload } = action
            const items = [...state.items]
            items.find((ele, index) => {
                if(ele._id === payload.item._id) {
                    items[index] = payload.item
                }
            })
            return({
                ...state,
                items: items,
            })
        },
        createItem: (state, action) => {
            const { payload } = action
            const items = [...state.items, payload.item]
            return({
                ...state,
                items: items,
            })
        },
        deleteItem: (state, action) => {
            const { payload } = action
            const items = [...state.items]
            const itemIndex = items.findIndex((item) => item._id === payload.itemId)
            items.splice(itemIndex, 1)
            return({
                ...state,
                items: items,
            })
        }
    },
});

export const { getItems, setSelectedProduct, updateItem, createItem, deleteItem } = table.actions;

export default table.reducer;
